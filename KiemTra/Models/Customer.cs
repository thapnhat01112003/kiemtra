﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        [Required, StringLength(100)]
        public string FirstName { get; set; }
        [Range(0.01, 10000.00)]
        public string LastName { get; set; }
        [Range(0.01, 10000.00)]
        public decimal Contactandaddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int addtexthere { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Reports
    {

        public int ReportsID { get; set; }
        [Required, StringLength(100)]
        public int AccountID { get; set; }
        [Required, StringLength(100)]
        public int LogsID { get; set; }
        [Required, StringLength(100)]
        public int TransactionalID { get; set; }
        [Required, StringLength(100)]
        public string ReportName { get; set; }
        [Range(0.01, 10000.00)]
        public string reportDate { get; set; }
        [Range(0.01, 10000.00)]
        public string Addtexthere { get; set; }
      
    }
}

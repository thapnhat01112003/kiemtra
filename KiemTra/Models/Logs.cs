﻿namespace KiemTra.Models
{
    public class Logs
    {
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public string LoginDate{ get; set; } 
        public string LoginTime {  get; set; }
        public string Addtexthere { get; set;}
    }
}
